using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Supercompany.Core;
using UnityEditor;
using UnityEngine;

namespace Supercompany.CrossPromo
{
    public class CrossPromoTextureCache
    {
        public static string CacheFolder = $"{Application.temporaryCachePath}/{nameof(CrossPromoTextureCache)}";
        private RemoteFileCache _fileCache;
        private Dictionary<string, Texture2D> _iconCache = new Dictionary<string, Texture2D>();
        private Dictionary<string, Texture2D> _bannerCache = new Dictionary<string, Texture2D>();

        public CrossPromoTextureCache()
        {
            _fileCache = new RemoteFileCache(CacheFolder);
        }

        public async Task<(bool, bool)> PreCacheTextures(CrossPromoInfo mainAppInfo, IEnumerable<CrossPromoInfo> appInfos, Orientation screenOrientation, CancellationToken cancellationToken)
        {
            bool haveBanner = false;
            bool haveAnyIcon = false;
            if (!string.IsNullOrEmpty(mainAppInfo?.Id))
            {
                haveBanner = await _fileCache.DownloadFileIfNotCachedAsync(mainAppInfo.GetBannerCacheFileName(screenOrientation), mainAppInfo.GetBannerUrl(screenOrientation), cancellationToken);
            }
            foreach (CrossPromoInfo appInfo in appInfos)
            {
                if (string.IsNullOrEmpty(appInfo?.Id))
                {
                    continue;
                }
                bool haveIcon = await _fileCache.DownloadFileIfNotCachedAsync(appInfo.IconCacheFileName, appInfo.IconUrl, cancellationToken);
                haveAnyIcon = haveAnyIcon || haveIcon;
            }
            return (haveBanner, haveAnyIcon);
        }

        public async Task<Texture2D> GetIconAsync(CrossPromoInfo info, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(info?.Id))
            {
                return null;
            }
            if (!_iconCache.TryGetValue(info.Id, out Texture2D texture))
            {
                texture = await _fileCache.GetFileTextureAsync(info.IconCacheFileName, info.IconUrl, cancellationToken: cancellationToken);
                _iconCache[info.Id] = texture;
            }
            return texture;
        }

        public async Task<Texture2D> GetPromoBannerAsync(CrossPromoInfo info, Orientation orientation, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(info?.Id))
            {
                return null;
            }
            if (!_bannerCache.TryGetValue(info.Id, out Texture2D texture))
            {
                texture = await _fileCache.GetFileTextureAsync(info.GetBannerCacheFileName(orientation), info.GetBannerUrl(orientation), cancellationToken: cancellationToken);
                _bannerCache[info.Id] = texture;
            }
            return texture;
        }

        public void ClearMemoryCache()
        {
            ClearIconMemoryCache();
            ClearBannerMemoryCache();
        }

        public void ClearIconMemoryCache()
        {
            foreach (Texture2D texture in _iconCache.Values)
            {
                Object.Destroy(texture);
            }
            _iconCache.Clear();
        }

        public void ClearBannerMemoryCache()
        {
            foreach (Texture2D texture in _bannerCache.Values)
            {
                Object.Destroy(texture);
            }
            _bannerCache.Clear();
        }

#if UNITY_EDITOR
        [MenuItem("Tools/Supercompany Cross-Promo/Reveal Texture Cache Folder")]
        static void RevealCacheFolderInFinder()
        {
            EditorUtility.RevealInFinder(CacheFolder);
        }
#endif
    }
}
