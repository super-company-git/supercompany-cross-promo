using System;
using System.IO;
using UnityEngine;

namespace Supercompany.CrossPromo
{
    [Serializable]
    public class CrossPromoInfo
    {
        [Tooltip("App Identifier.\n"
            + "Will never be presented directly to users.\n"
            + "It is used for matching main app ids.\n"
            + "It is used in analytics events as the 'game' parameter.\n"
            + "It is used by the texture caching system.")]
        public string Id;
        [Tooltip("App title, displayed in the 'all apps' screen.")]
        public string Title;
        [Tooltip("Must be a valid URL pointing to a valid texture, or this app will not appear in the 'all apps' screen.")]
        public string IconUrl;
        [Tooltip("Must be a valid URL pointing to a valid texture.\n"
            + "This is used only on the 'main app' screen on portrait orientation.\n"
            + "Its aspect should be 1:1.")]
        public string PortraitBannerUrl;
        [Tooltip("Must be a valid URL pointing to a valid texture.\n"
            + "This is used only on the 'main app' screen on landscape orientation.\n"
            + "Its aspect should be 12:7.")]
        public string LandscapeBannerUrl;
        [Tooltip("Android app package name.\n"
            + "Used for opening Google Play Store intent in Android devices.")]
        public string AndroidPackage;
        [Tooltip("iOS iTunes app id.\n"
            + "Used for opening Apple App Store in iOS devices, as well as showing app promotion overlay on portrait orientation.")]
        public string IosItunesId;
        [Tooltip("URL that will be opened when tapping the 'play now' button in the 'main app' screen or tapping any icon in 'all apps' screen.\n"
            + "On Android this URL is not used.\n"
            + "On iOS, if the URL is null but the iTunes id is not, an App Store URL will be used as fallback.")]
        public string Url;
        [Tooltip("URL Scheme supported by the app."
#if HAVE_URL_SCHEME
            + "\nThis is used to verify if the app is installed in the device by querying if a URL with this scheme can be opened."
#endif
            )]
        public string AppUrlScheme;

        public string AndroidInAppLinkUrl => string.IsNullOrEmpty(AndroidPackage) ? null : $"market://details?id={AndroidPackage}";
        public string GooglePlayStoreUrl => string.IsNullOrEmpty(AndroidPackage) ? null : $"https://play.google.com/store/apps/details?id={AndroidPackage}";
        public string ITunesStoreUrl => string.IsNullOrEmpty(IosItunesId) ? null : $"https://apps.apple.com/app/id{IosItunesId}";

#if UNITY_ANDROID
        public string StoreUrl => !string.IsNullOrEmpty(Url) ? Url : GooglePlayStoreUrl;
#elif UNITY_IOS || UNITY_STANDALONE_OSX
        public string StoreUrl => !string.IsNullOrEmpty(Url) ? Url : ITunesStoreUrl;
#else
        public string StoreUrl => Url;
#endif

        public string IconCacheFileName => Path.ChangeExtension(Id, "png");

        public string GetBannerCacheFileName(Orientation orientation)
        {
            switch (orientation)
            {
                case Orientation.Portrait: return Path.ChangeExtension(Id, "banner.portrait.png");
                case Orientation.Landscape: return Path.ChangeExtension(Id, "banner.landscape.png");
                default: throw new ArgumentOutOfRangeException(nameof(orientation));
            }
        }

        public string GetBannerUrl(Orientation orientation)
        {
            switch (orientation)
            {
                case Orientation.Portrait: return PortraitBannerUrl ?? LandscapeBannerUrl;
                case Orientation.Landscape: return LandscapeBannerUrl ?? PortraitBannerUrl;
                default: throw new ArgumentOutOfRangeException(nameof(orientation));
            }
        }

        public bool IsAppInstalled
        {
            get
            {
#if HAVE_URL_SCHEME
                return !string.IsNullOrEmpty(AppUrlScheme) && Supercompany.UrlScheme.UrlQuery.CanOpenUrl(AppUrlScheme);
#else
                return false;
#endif
            }
        }
    }
}
