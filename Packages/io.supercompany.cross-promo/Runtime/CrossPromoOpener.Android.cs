#if UNITY_ANDROID
using UnityEngine;

namespace Supercompany.CrossPromo
{
    public static partial class CrossPromoOpener
    {
        private static bool OpenCrossPromoAndroid(CrossPromoInfo info)
        {
#if !UNITY_EDITOR
            if (!string.IsNullOrEmpty(info.AndroidPackage))
            {
                using (var unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
                using (var context = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity"))
                using (var crossPromoAndroidClass = new AndroidJavaClass("io.supercompany.crosspromo.CrossPromoAndroid"))
                {
                    return crossPromoAndroidClass.CallStatic<bool>("open", context, info.AndroidPackage);
                }
            }
#endif
            return false;
        }
    }
}
#endif