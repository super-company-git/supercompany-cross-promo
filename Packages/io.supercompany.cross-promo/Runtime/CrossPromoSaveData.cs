using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Supercompany.CrossPromo
{
    [Serializable]
    public class CrossPromoSaveData
    {
        public static string SaveDataPath => $"{Application.persistentDataPath}/Supercompany.CrossPromo.data";
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        public const bool PrettyPrint = true;
#else
        public const bool PrettyPrint = false;
#endif

        public bool OpenedOnce;
        public string LastMainAppId;

        private bool _needsSave = false;
        private bool _isSaveRoutineRunning = false;

        #region Save/Load

        public static Task<CrossPromoSaveData> LoadAsync(CancellationToken cancellationToken)
        {
            return LoadFromFileAsync(SaveDataPath, cancellationToken);
        }

        public static async Task<CrossPromoSaveData> LoadFromFileAsync(string filePath, CancellationToken cancellationToken)
        {
            if (!File.Exists(filePath))
            {
                return null;
            }

#if UNITY_2021_2_OR_NEWER
            string contents = await File.ReadAllTextAsync(filePath, cancellationToken);
#else
            string contents = await Task.Run(() => File.ReadAllText(filePath));
#endif
            return JsonUtility.FromJson<CrossPromoSaveData>(contents);
        }

        public Task SaveAsync(CancellationToken cancellationToken = default)
        {
            return SaveToFileAsync(SaveDataPath, cancellationToken);
        }

        public async Task SaveToFileAsync(string filePath, CancellationToken cancellationToken = default)
        {
            _needsSave = true;
            if (_isSaveRoutineRunning || string.IsNullOrEmpty(filePath))
            {
                return;
            }

            _isSaveRoutineRunning = true;
            try
            {
                await SaveRoutineAsync(filePath, cancellationToken);
            }
            finally
            {
                _isSaveRoutineRunning = false;
            }
        }

        private async Task SaveRoutineAsync(string filePath, CancellationToken cancellationToken = default)
        {
            while (_needsSave)
            {
                _needsSave = false;
#if !UNITY_2021_2_OR_NEWER
                await File.WriteAllTextAsync(filePath, JsonUtility.ToJson(this, PrettyPrint), cancellationToken);
#else
                await Task.Run(() => File.WriteAllText(filePath, JsonUtility.ToJson(this, PrettyPrint)), cancellationToken);
#endif
            }
        }

        #endregion

#if UNITY_EDITOR
        #region Editor-only stuff

        [MenuItem("Tools/Supercompany Cross-Promo/Reveal Save Data")]
        public static void RevealSaveFileInFinder()
        {
            EditorUtility.RevealInFinder(SaveDataPath);
        }

        #endregion
#endif
    }
}
