#if UNITY_IOS
using System.Runtime.InteropServices;

namespace Supercompany.CrossPromo
{
    public static partial class CrossPromoOpener
    {
        [DllImport("__Internal", CharSet = CharSet.Ansi)]
        public static extern bool CrossPromoIos_ShowOverlay(string iTunesId, bool userDismissible);

        [DllImport("__Internal")]
        public static extern void CrossPromoIos_DismissOverlay();

        private static bool ShowOverlayIos(CrossPromoInfo info, bool userDismissible = true)
        {
#if !UNITY_EDITOR
            if (!string.IsNullOrEmpty(info.IosItunesId))
            {
                return CrossPromoIos_ShowOverlay(info.IosItunesId, userDismissible);
            }
#endif
            return false;
        }

        private static void DismissOverlayIos()
        {
#if !UNITY_EDITOR
            CrossPromoIos_DismissOverlay();
#endif
        }
    }
}
#endif