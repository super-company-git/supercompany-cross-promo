using System;
using System.Collections.Generic;
using Supercompany.Core;
using UnityEngine;

namespace Supercompany.CrossPromo
{
    [Serializable]
    public class CrossPromoConfig
    {
        [Tooltip("App infos with a valid Id and IconUrl will appear in the 'all apps' screen.\n"
            + "The 'main app' screen will only appear if the id in MainAppIds match any of the apps configured in this list.")]
        public List<CrossPromoInfo> AppInfos;

        [Tooltip("List of ids from 'App Infos' that should appear in the 'main screen'.\n"
            + "Only the first id matched will appear in the 'main app' screen.\n"
#if HAVE_URL_SCHEME
            + "Apps that are found to be installed in the device, via their 'App Url Scheme', are not matched.\n"
#endif
            + "If there is no match, the 'main app' screen will be skipped and 'all apps' screen will appear instead.")]
        public List<string> MainAppIds;

        public bool RandomizeMainApp;

        public CrossPromoInfo MainApp
        {
            get
            {
                if (!MainAppIds.IsNullOrEmpty())
                {
                    if (RandomizeMainApp)
                        MainAppIds.Shuffle();

                    foreach (string id in MainAppIds)
                    {
                        if (AppInfos.Find(appInfo => appInfo.Id == id) is CrossPromoInfo appInfo && !appInfo.IsAppInstalled)
                        {
                            return appInfo;
                        }
                    }
                }
                return null;
            }
        }
    }
}
