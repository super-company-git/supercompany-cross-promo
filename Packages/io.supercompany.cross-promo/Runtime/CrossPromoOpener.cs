using System.Diagnostics;
using UnityEngine;

namespace Supercompany.CrossPromo
{
    public static partial class CrossPromoOpener
    {
        public static bool OpenCrossPromo(CrossPromoInfo info)
        {
            if (info == null)
            {
                return false;
            }

#if UNITY_ANDROID
            if (OpenCrossPromoAndroid(info))
            {
                return true;
            }
#endif

            string url = info.StoreUrl;
            if (!string.IsNullOrEmpty(url))
            {
                Application.OpenURL(url);
                return true;
            }
            else
            {
                return false;
            }
        }

        [Conditional("UNITY_IOS")]
        public static void ShowOverlay(CrossPromoInfo appInfo)
        {
#if UNITY_IOS
            ShowOverlayIos(appInfo);
#endif
        }

        [Conditional("UNITY_IOS")]
        public static void DismissOverlay()
        {
#if UNITY_IOS
            DismissOverlayIos();
#endif
        }
    }
}
