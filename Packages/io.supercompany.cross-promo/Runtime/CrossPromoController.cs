using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Supercompany.Core;
#if SUPERCOMPANY_REMOTECONFIG
using Supercompany.RemoteConfig;
#endif
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

namespace Supercompany.CrossPromo
{
    public class CrossPromoController : MonoBehaviour
    {
        public event Action OnPanelOpenedEvent;
        public event Action OnPanelClosedEvent;

        public const string CrossPromoConfigRemoteConfig = "CrossPromoConfig";

        public CrossPromoConfig CrossPromoConfig;
        [Tooltip("If true, the Cross-Promo system will setup automatically on this object's Start message.\n"
            + "Otherwise you should call one of the SetupAsync methods manually.")]
        public bool SetupOnStart = true;
        [Tooltip("If true, if the Cross-Promo popup was never opened, it will be opened automatically as soon as soon as the setup finishes\n"
            + "Otherwise, if you want to open the popup programatically, call the OpenPanel or OpenPanelCoroutine methods.")]
        public bool AutoOpenOnce = false;
#if SUPERCOMPANY_REMOTECONFIG
        [Tooltip("If true, and there is a remote config named 'CrossPromoConfig', it will be loaded and override the configuration made in the Inspector when calling SetupAsync().")]
        public bool UseRemoteConfig = true;
#endif

        [Header("UI objects")]
        [SerializeField] protected UIDocument _uiDocument;
        [SerializeField] protected VisualTreeAsset _mainScreenTemplate;
        [SerializeField] protected VisualTreeAsset _appListTemplate;
        [SerializeField] protected VisualTreeAsset _appCellTemplate;

        [Header("Ready events")]
        [Tooltip("This will be called when the Cross-Promo popup is ready to be displayed.\n"
            + "It is the perfect event for showing the Cross-Promo button or opening the Cross-Promo popup automatically by code.")]
        public UnityEvent OnCrossPromoReady;
        [Tooltip("This will be called when the Cross-Promo popup is not ready to be displayed, at this object's Start message.\n"
            + "You should hide the Cross-Promo button here.")]
        public UnityEvent OnCrossPromoNotReady;

        [Header("Badge events")]
        [Tooltip("This will be called when there should be a 'news' badge icon displayed on top of the Cross-Promo button.")]
        public UnityEvent ShowBadgeEvent;
        [Tooltip("This will be called when the 'news' badge icon displayed on top of the Cross-Promo button should be hidden.")]
        public UnityEvent HideBadgeEvent;

        public bool IsReadyToOpen => _haveAnyIcon;
        public bool IsOpened { get; private set; } = false;
        public bool WasNeverOpened => _saveData == null || !_saveData.OpenedOnce;

        protected VisualElement _mainScreenRoot;
        protected VisualElement _appListRoot;
        protected CrossPromoTextureCache _textureCache;
        protected CancellationToken CancellationToken
        {
            get
            {
                if (_cancellationTokenSource == null)
                {
                    _cancellationTokenSource = new CancellationTokenSource();
                }
                return _cancellationTokenSource.Token;
            }
        }
        private CancellationTokenSource _cancellationTokenSource;
        protected bool _haveBannerTexture { get; private set; }
        protected bool _haveAnyIcon { get; private set; }
        protected CrossPromoInfo _mainAppInfo { get; private set; }
        protected CrossPromoSaveData _saveData { get; private set; }

        void Awake()
        {
            _textureCache = new CrossPromoTextureCache();
        }

        void Start()
        {
            OnCrossPromoNotReady.Invoke();
            HideBadgeEvent.Invoke();
            if (SetupOnStart)
            {
                SetupAsync().Forget();
            }
        }

        void OnDestroy()
        {
            CancelTokenSource();
        }

#if SUPERCOMPANY_REMOTECONFIG
        public async Task SetupAsync(RemoteConfigManager remoteConfigManager)
        {
            await remoteConfigManager.WaitUntilInitialized(CancellationToken);
            await SetupAsync(RemoteConfigManager.Instance.GetObject(CrossPromoConfigRemoteConfig, defaultValue: CrossPromoConfig));
        }
#endif

        public Task SetupAsync(CrossPromoConfig crossPromoConfig)
        {
            CrossPromoConfig = crossPromoConfig;
            return SetupInternalAsync();
        }

        public Task SetupAsync()
        {
#if SUPERCOMPANY_REMOTECONFIG
            if (UseRemoteConfig)
            {
                return SetupAsync(RemoteConfigManager.Instance);
            }
            else
#endif
            {
                return SetupInternalAsync();
            }
        }

        public void OpenPanel()
        {
            if (IsOpened)
            {
                return;
            }

            if (_haveBannerTexture)
            {
                OpenMainScreenAsync().Forget();
            }
            else if (_haveAnyIcon)
            {
                OpenAppList();
            }
            else
            {
                Debug.LogWarning($"[{nameof(CrossPromoController)}] Trying to open Cross-Promo panel before setup finished", this);
                return;
            }

            IsOpened = true;

            if (!_saveData.OpenedOnce)
            {
                _saveData.OpenedOnce = true;
                _saveData.SaveAsync().Forget();
            }

            OnPanelOpenedEvent?.Invoke();
        }

        public CustomYieldInstruction OpenPanelCoroutine()
        {
            OpenPanel();
            return new WaitUntil(() => IsOpened);
        }

        public void ClosePanel()
        {
            IsOpened = false;
            CancelTokenSource();
            ClearOpenScreens();
            _uiDocument.enabled = false;
            OnPanelClosedEvent?.Invoke();
        }

        private async Task SetupInternalAsync()
        {
            if (CrossPromoConfig != null)
            {
                _mainAppInfo = CrossPromoConfig.MainApp;
                (_haveBannerTexture, _haveAnyIcon) = await _textureCache.PreCacheTextures(_mainAppInfo, CrossPromoConfig.AppInfos, OrientationUtils.CurrentScreenOrientation, CancellationToken);
            }
            else
            {
                _mainAppInfo = null;
                _haveBannerTexture = _haveAnyIcon = false;
            }

            if (!IsReadyToOpen)
            {
                return;
            }

            if (_saveData == null)
            {
                await LoadSaveAsync();
            }
            OnCrossPromoReady.Invoke();
            if (AutoOpenOnce && WasNeverOpened)
            {
                OpenPanel();
            }
            else if (!string.IsNullOrEmpty(_mainAppInfo?.Id) && _mainAppInfo.Id != _saveData.LastMainAppId)
            {
                ShowBadgeEvent.Invoke();
            }
        }

        private async Task LoadSaveAsync()
        {
            try
            {
                _saveData = await CrossPromoSaveData.LoadAsync(CancellationToken);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex, this);
            }
            finally
            {
                if (_saveData == null)
                {
                    _saveData = new CrossPromoSaveData();
                }
            }
        }

        private async Task OpenMainScreenAsync()
        {
            ClearOpenScreens();
            if (!_haveBannerTexture)
            {
                return;
            }

            Orientation screenOrientation = OrientationUtils.CurrentScreenOrientation;
            _uiDocument.panelSettings.SetReferenceResolution(screenOrientation);

            Texture2D bannerTexture = await _textureCache.GetPromoBannerAsync(_mainAppInfo, screenOrientation, CancellationToken);

            _uiDocument.visualTreeAsset = _mainScreenTemplate;
            _uiDocument.enabled = true;
            _mainScreenRoot = _uiDocument.rootVisualElement;
            _mainScreenRoot.focusable = true;
            _mainScreenRoot.Focus();
            _mainScreenRoot.AddToClassList(screenOrientation.ToString().ToLowerInvariant());
            SetMainTexture(bannerTexture);
            SetupMainScreenEvents();

            CrossPromoAnalytics.LogMainScreenImpression(_mainAppInfo);
            if (screenOrientation == Orientation.Portrait)
            {
                CrossPromoOpener.ShowOverlay(_mainAppInfo);
            }

            _saveData.LastMainAppId = _mainAppInfo.Id;
            _saveData.SaveAsync().Forget();
            HideBadgeEvent.Invoke();
        }

        private void OpenAppList()
        {
            ClearOpenScreens();
            if (!_haveAnyIcon)
            {
                return;
            }

            _uiDocument.visualTreeAsset = _appListTemplate;
            _uiDocument.enabled = true;
            _appListRoot = _uiDocument.rootVisualElement;
            _appListRoot.focusable = true;
            _appListRoot.Focus();
            CreateAppCellsAsync().Forget();
            SetupAppListEvents();

            CrossPromoAnalytics.LogAppListImpression();
        }

        private void SetMainTexture(Texture2D texture)
        {
            if (_mainScreenRoot.Q(className: "main-promo__banner") is VisualElement banner)
            {
                banner.style.backgroundImage = texture;
            }
        }

        private void ClearOpenScreens()
        {
            UnregisterMainScreenEvents();
            _mainScreenRoot = null;
            UnregisterAppListEvents();
            UnregisterAppCellEvents();
            _appListRoot = null;

            CrossPromoOpener.DismissOverlay();

            _textureCache.ClearMemoryCache();
        }

        private void CancelTokenSource()
        {
            _cancellationTokenSource?.Cancel();
            _cancellationTokenSource?.Dispose();
            _cancellationTokenSource = null;
        }

        #region Main Screen events

        private void SetupMainScreenEvents()
        {
            _mainScreenRoot.RegisterCallback<KeyUpEvent>(OnMainScreenBack);
            if (_mainScreenRoot.Q(name: "button-play-now") is VisualElement playNowButton)
            {
                playNowButton.RegisterCallback<ClickEvent>(OnPlayNow);
            }
            if (_mainScreenRoot.Q(name: "banner") is VisualElement banner)
            {
                banner.RegisterCallback<ClickEvent>(OnPlayNow);
            }
            if (_mainScreenRoot.Q(name: "button-back") is VisualElement backButton)
            {
                backButton.RegisterCallback<ClickEvent>(OnMainScreenBack);
            }
            if (_mainScreenRoot.Q(name: "button-more-games") is VisualElement moreGamesButton)
            {
                moreGamesButton.RegisterCallback<ClickEvent>(OnMoreGames);
            }
        }

        private void UnregisterMainScreenEvents()
        {
            if (_mainScreenRoot == null)
            {
                return;
            }

            _mainScreenRoot.UnregisterCallback<KeyUpEvent>(OnMainScreenBack);
            if (_mainScreenRoot.Q(name: "button-play-now") is VisualElement playNowButton)
            {
                playNowButton.UnregisterCallback<ClickEvent>(OnPlayNow);
            }
            if (_mainScreenRoot.Q(name: "banner") is VisualElement banner)
            {
                banner.UnregisterCallback<ClickEvent>(OnPlayNow);
            }
            if (_mainScreenRoot.Q(name: "button-back") is VisualElement backButton)
            {
                backButton.UnregisterCallback<ClickEvent>(OnMainScreenBack);
            }
            if (_mainScreenRoot.Q(name: "button-more-games") is VisualElement moreGamesButton)
            {
                moreGamesButton.UnregisterCallback<ClickEvent>(OnMoreGames);
            }
        }

        private void OnPlayNow(ClickEvent _)
        {
            CrossPromoOpener.OpenCrossPromo(_mainAppInfo);
            CrossPromoAnalytics.LogMainScreenClick(_mainAppInfo);
        }

        private void OnMainScreenBack(ClickEvent _)
        {
            ClosePanel();
        }

        private void OnMainScreenBack(KeyUpEvent evt)
        {
            if (evt.keyCode == KeyCode.Escape)
            {
                ClosePanel();
                evt.StopPropagation();
            }
        }

        private void OnMoreGames(ClickEvent _)
        {
            OpenAppList();
        }

        #endregion

        #region App List events

        private void SetupAppListEvents()
        {
            _appListRoot.RegisterCallback<KeyUpEvent>(OnAppListBack);
            if (_appListRoot.Q(name: "button-back") is VisualElement backButton)
            {
                backButton.RegisterCallback<ClickEvent>(OnAppListBack);
            }
        }

        private void UnregisterAppListEvents()
        {
            if (_appListRoot == null)
            {
                return;
            }

            _appListRoot.UnregisterCallback<KeyUpEvent>(OnAppListBack);
            if (_appListRoot.Q(name: "button-back") is VisualElement backButton)
            {
                backButton.UnregisterCallback<ClickEvent>(OnAppListBack);
            }
        }

        private void OnAppListBack(ClickEvent _)
        {
            OnAppListBack();
        }

        private void OnAppListBack(KeyUpEvent evt)
        {
            if (evt.keyCode == KeyCode.Escape)
            {
                OnAppListBack();
                evt.StopPropagation();
            }
        }

        private void OnAppListBack()
        {
            if (_haveBannerTexture)
            {
                OpenMainScreenAsync().Forget();
            }
            else
            {
                ClosePanel();
            }
        }

        #endregion

        #region App List cell

        private async Task CreateAppCellsAsync()
        {
            if (_appListRoot.Q<ScrollView>(name: "app-list") is ScrollView viewport)
            {
                VisualElement container = viewport.contentContainer;
                foreach (CrossPromoInfo appInfo in CrossPromoConfig.AppInfos.OrderBy(appInfo => appInfo.IsAppInstalled))
                {
                    var texture = await _textureCache.GetIconAsync(appInfo, CancellationToken);
                    if (texture == null)
                    {
                        continue;
                    }

                    VisualElement cell = _appCellTemplate.Instantiate();
                    if (cell.Q(className: "app-cell__icon") is VisualElement icon)
                    {
                        icon.style.backgroundImage = texture;
                    }
                    if (cell.Q<Label>(className: "app-cell__label") is Label label)
                    {
                        label.text = appInfo.Title;
                    }
                    cell.RegisterCallback<ClickEvent, CrossPromoInfo>(OnAppCellClick, appInfo);
                    cell.visible = false;
                    container.Add(cell);
                }

                if (container.childCount == 0)
                {
                    return;
                }
                else if (container.childCount == 1)
                {
                    // we need that least one frame passed for first cell to be layouted and have its "width" available
                    await Task.Yield();
                }

                float cellWidth = container[0].resolvedStyle.width;
                float containerWidth = container.resolvedStyle.width;
                int cellCount = Mathf.FloorToInt(containerWidth / cellWidth);
                float horizontalMargin = containerWidth - (cellWidth * cellCount);
                container.style.paddingLeft = horizontalMargin * 0.5f;

                foreach (VisualElement cell in container.Children())
                {
                    cell.visible = true;
                }
            }
        }

        private void UnregisterAppCellEvents()
        {
            if (_appListRoot == null)
            {
                return;
            }

            foreach (VisualElement cell in _appListRoot.Query(className: "app-cell").Build())
            {
                cell.UnregisterCallback<ClickEvent, CrossPromoInfo>(OnAppCellClick);
            }
        }

        private void OnAppCellClick(ClickEvent _, CrossPromoInfo appInfo)
        {
            CrossPromoOpener.OpenCrossPromo(appInfo);
            CrossPromoAnalytics.LogAppListClick(appInfo);
        }

        #endregion
    }
}
