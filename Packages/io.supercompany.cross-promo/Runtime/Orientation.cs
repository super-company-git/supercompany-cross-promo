using UnityEngine;
using UnityEngine.UIElements;

namespace Supercompany.CrossPromo
{
    public enum Orientation
    {
        Portrait,
        Landscape,
    }

    public static class OrientationUtils
    {
        public static Orientation CurrentScreenOrientation => new Vector2Int(Screen.width, Screen.height).GetOrientation();

        public static Orientation GetOrientation(this Vector2Int v)
        {
            return v.x > v.y ? Orientation.Landscape : Orientation.Portrait;
        }

        public static void SetReferenceResolution(this PanelSettings panelSettings, Orientation orientation)
        {
            Vector2Int resolution = panelSettings.referenceResolution;
            if (resolution.GetOrientation() != orientation)
            {
                panelSettings.referenceResolution = new Vector2Int(resolution.y, resolution.x);
            }
        }
    }
}
