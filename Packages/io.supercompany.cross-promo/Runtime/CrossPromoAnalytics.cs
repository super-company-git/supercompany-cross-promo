using System.Collections.Generic;
using System.Diagnostics;
#if SUPERCOMPANY_ANALYTICS
using Supercompany.Analytics;
#endif

namespace Supercompany.CrossPromo
{
    public static class CrossPromoAnalytics
    {
        [Conditional("SUPERCOMPANY_ANALYTICS")]
        public static void LogMainScreenImpression(CrossPromoInfo appInfo)
        {
            LogEvent("cp_main_impr", new Dictionary<string, object>
            {
                ["game"] = appInfo.Id,
            });
        }

        [Conditional("SUPERCOMPANY_ANALYTICS")]
        public static void LogAppListImpression()
        {
            LogEvent("cp_all_impr");
        }

        [Conditional("SUPERCOMPANY_ANALYTICS")]
        public static void LogMainScreenClick(CrossPromoInfo appInfo)
        {
            LogEvent("cp_click_game_main", new Dictionary<string, object>
            {
                ["game"] = appInfo.Id,
            });
        }

        [Conditional("SUPERCOMPANY_ANALYTICS")]
        public static void LogAppListClick(CrossPromoInfo appInfo)
        {
            LogEvent("cp_click_game_all", new Dictionary<string, object>
            {
                ["game"] = appInfo.Id,
            });
        }

        [Conditional("SUPERCOMPANY_ANALYTICS")]
        private static void LogEvent(string name)
        {
#if SUPERCOMPANY_ANALYTICS
            AnalyticsManager.Instance.LogEvent(name);
#endif
        }

        [Conditional("SUPERCOMPANY_ANALYTICS")]
        private static void LogEvent(string name, Dictionary<string, object> parameters)
        {
#if SUPERCOMPANY_ANALYTICS
            AnalyticsManager.Instance.LogEvent(name, parameters);
#endif
        }
    }
}