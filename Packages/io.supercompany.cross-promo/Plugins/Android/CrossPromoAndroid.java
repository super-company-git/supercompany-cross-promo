package io.supercompany.crosspromo;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

public class CrossPromoAndroid {
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public static boolean open(Context context, String androidPackage) {
        if (androidPackage == null || androidPackage.isEmpty()) {
            return false;
        }

        try {
            Intent appStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + androidPackage));
            appStoreIntent.setPackage("com.android.vending");
            context.startActivity(appStoreIntent);
        } catch (ActivityNotFoundException exception) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + androidPackage)));
        }
        return true;
    }
}