#include <StoreKit/StoreKit.h>

bool CrossPromoIos_ShowOverlay(const char *iTunesId, bool userDismissible) {
    if (@available(iOS 14, macCatalyst 14, *)) {
        if (iTunesId == NULL) {
            return NO;
        }

        UIWindowScene *scene = UIApplication.sharedApplication.delegate.window.windowScene;
        if (scene == nil) {
            return NO;
        }

        NSString *appId = [NSString stringWithUTF8String:iTunesId];
        SKOverlayAppConfiguration *config = [[SKOverlayAppConfiguration alloc] initWithAppIdentifier:appId position:SKOverlayPositionBottom];
        config.userDismissible = userDismissible;
        SKOverlay *overlay = [[SKOverlay alloc] initWithConfiguration:config];
        [overlay presentInScene:scene];
        return YES;
    }
    return NO;
}

void CrossPromoIos_DismissOverlay(void) {
    if (@available(iOS 14, macCatalyst 14, *)) {
        UIWindowScene *scene = UIApplication.sharedApplication.delegate.window.windowScene;
        if (scene != nil) {
            [SKOverlay dismissOverlayInScene:scene];
        }
    }
}
