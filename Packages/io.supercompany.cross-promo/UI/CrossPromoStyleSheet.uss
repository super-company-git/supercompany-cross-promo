Label {
    color: rgb(255, 255, 255);
}

Label.bold {
    -unity-font: url('project://database/Packages/io.supercompany.cross-promo/UI/Fonts/Inter-Bold.ttf?fileID=12800000&guid=8f4058b79356d490cba2a5099e0ed352&type=3#Inter-Bold');
    -unity-font-definition: initial;
}

Label.semibold {
    -unity-font: url('project://database/Packages/io.supercompany.cross-promo/UI/Fonts/Inter-SemiBold.ttf?fileID=12800000&guid=6fc3d89cb3c1347349b7bfff08ddaee7&type=3#Inter-SemiBold');
    -unity-font-definition: initial;
}

.fill-parent {
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
}

.background-overlay {
    background-color: rgba(0, 0, 0, 0.5);
}

.company-logo {
    background-image: url('project://database/Packages/io.supercompany.cross-promo/UI/Textures/CompanyLogo.png?fileID=2800000&guid=a4d53c0c44d204f619c6295b66b85926&type=3#CompanyLogo');
    -unity-background-scale-mode: scale-to-fit;
}

.rounded-button {
    border-top-left-radius: 16px;
    border-bottom-left-radius: 16px;
    border-top-right-radius: 16px;
    border-bottom-right-radius: 16px;
    background-color: rgb(255, 255, 255);
    margin-left: -4px;
    margin-top: -4px;
    width: 100%;
    height: 100%;
    flex-direction: row;
    justify-content: center;
}

.rounded-button__shadow {
    border-top-left-radius: 16px;
    border-bottom-left-radius: 16px;
    border-top-right-radius: 16px;
    border-bottom-right-radius: 16px;
    background-color: rgba(0, 0, 0, 0.25);
    margin-left: 4px;
    margin-top: 4px;
    width: 100%;
    height: 100%;
}

.button-gray.rounded-button {
    background-color: rgb(128, 144, 141);
}

.button-green.rounded-button {
    background-color: rgb(120, 204, 100);
}

.main-promo {
    background-color: rgb(100, 100, 100);
    border-left-color: rgb(108, 108, 108);
    border-right-color: rgb(108, 108, 108);
    border-top-color: rgb(108, 108, 108);
    border-bottom-color: rgb(108, 108, 108);
    border-left-width: 10px;
    border-right-width: 10px;
    border-top-width: 10px;
    border-bottom-width: 10px;
    border-top-left-radius: 20px;
    border-bottom-left-radius: 20px;
    border-top-right-radius: 20px;
    border-bottom-right-radius: 20px;
}

.portrait .main-promo {
    padding-left: 55px;
    padding-right: 55px;
    width: 936px;
    max-width: 90%;
    height: 1453px;
    max-height: 90%;
    padding-top: 0;
    padding-bottom: 60px;
}

.landscape .main-promo {
    padding-left: 110px;
    padding-right: 110px;
    padding-top: 0;
    padding-bottom: 32px;
    width: 1302px;
    max-width: 90%;
    height: 948px;
    max-height: 90%;
}

.portrait .main-promo-holder {
    left: 0;
    right: 0;
    top: 0;
    bottom: 5%;
    justify-content: center;
    position: absolute;
    align-items: center;
}

.landscape .main-promo-holder {
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    justify-content: center;
    position: absolute;
    align-items: center;
}

.main-promo__company-logo-holder {
    flex-grow: 1;
    justify-content: center;
}

.portrait .main-promo__company-logo {
    height: 130px;
    max-height: 100%;
}

.landscape .main-promo__company-logo {
    height: 100px;
    max-height: 100%;
}

.main-promo__banner {
    border-top-left-radius: 40px;
    border-bottom-left-radius: 40px;
    border-top-right-radius: 40px;
    border-bottom-right-radius: 40px;
    -unity-background-scale-mode: scale-to-fit;
}

.portrait .main-promo__banner {
    padding-top: 100%;
}

.landscape .main-promo__banner {
    padding-top: 58%;
}

.portrait .main-promo__button-holder {
    margin-top: 70px;
    flex-basis: 280px;
}

.landscape .main-promo__button-holder {
    margin-top: 28px;
    flex-direction: row;
    flex-basis: 133px;
}

.portrait .main-promo__button-play-now {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 60%;
}

.landscape .main-promo__button-play-now {
    margin-left: 32px;
    margin-right: 32px;
    flex-grow: 1;
}

.portrait .main-promo__button-back {
    position: absolute;
    left: 0;
    top: 60%;
    right: 55%;
    bottom: 0;
}

.landscape .main-promo__button-back {
    flex-grow: 1;
}

.portrait .main-promo__button-more-games {
    position: absolute;
    left: 55%;
    top: 60%;
    right: 0;
    bottom: 0;
}

.landscape .main-promo__button-more-games {
    flex-grow: 1;
}

.main-promo__main-button__label {
    font-size: 45px;
    -unity-text-align: middle-center;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    margin-bottom: 0;
}

.main-promo__secondary-button__label {
    font-size: 35px;
    -unity-text-align: middle-center;
    white-space: normal;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    margin-bottom: 0;
}

.main-promo__back-button__icon {
    background-image: url('project://database/Packages/io.supercompany.cross-promo/UI/Textures/BackArrow.png?fileID=2800000&guid=44842fc37e39549bb8b699beb06eae44&type=3#BackArrow');
    -unity-background-scale-mode: scale-to-fit;
    width: 29px;
    margin-right: 20px;
}

.main-promo__more-button__icon {
    background-image: url('project://database/Packages/io.supercompany.cross-promo/UI/Textures/MoreGamesIcon.png?fileID=2800000&guid=8b8cbfc34402c411db3b0872a5eb28d3&type=3#MoreGamesIcon');
    -unity-background-scale-mode: scale-to-fit;
    width: 29px;
    margin-left: 20px;
}

.app-list__header {
    background-color: rgb(100, 100, 100);
    flex-basis: 167px;
}

.app-list__top-space {
    height: 5%;
}

.app-list__title {
    padding-top: 8px;
    padding-bottom: 8px;
}

.app-list__title__label {
    font-size: 35px;
    -unity-text-align: middle-center;
    padding-right: 16px;
}

.app-list__list {
    flex-grow: 1;
}

.app-list__list .unity-scroll-view__content-container {
    flex-wrap: wrap;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
}

.app-list__footer {
    flex-basis: 80px;
    align-items: center;
}

.app-list__back-button {
    height: 100%;
}

.app-list__back-button__icon {
    background-image: url('project://database/Packages/io.supercompany.cross-promo/UI/Textures/BackArrow.png?fileID=2800000&guid=44842fc37e39549bb8b699beb06eae44&type=3#BackArrow');
    -unity-background-scale-mode: scale-to-fit;
    width: 24px;
    margin-left: 32px;
    margin-right: 16px;
}

.app-list__back-button__label {
    -unity-text-align: middle-center;
    height: 100%;
    font-size: 25px;
    margin-left: 0;
    margin-right: 32px;
    margin-top: 0;
    margin-bottom: 0;
}

.app-list__background {
    background-color: rgb(0, 0, 0);
}

.app-cell {
    width: 215px;
    height: 275px;
    margin-left: 32px;
    margin-right: 32px;
    margin-top: 16px;
    margin-bottom: 16px;
}

.app-cell__icon {
    -unity-background-scale-mode: scale-to-fit;
    flex-grow: 1;
    border-top-left-radius: 20px;
    border-bottom-left-radius: 20px;
    border-top-right-radius: 20px;
    border-bottom-right-radius: 20px;
}

.app-cell__label {
    flex-basis: 60px;
    font-size: 23px;
    white-space: normal;
    -unity-text-align: upper-left;
}
