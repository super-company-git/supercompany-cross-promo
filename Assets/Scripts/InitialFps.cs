using UnityEngine;

public static class InitialFps
{
    [RuntimeInitializeOnLoadMethod]
    static void SetupInitialFps()
    {
        Application.targetFrameRate = 60;
    }
}
