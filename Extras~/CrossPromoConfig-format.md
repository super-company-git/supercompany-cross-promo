# CrossPromoConfig format

## `CrossPromoConfig` fields
- `AppInfos`: list of `CrossPromoInfo` objects.

  App infos with a valid `Id` and `IconUrl` will appear in the 'all apps' screen.

  The 'main app' screen will only appear if the id in `MainAppIds` match any of the apps configured in this list and it defines a valid texture URL in either `PortraitBannerUrl` or `LandscapeBannerUrl`.
- `MainAppIds`: list of ids from `AppInfos` that should appear in the 'main screen'.

  Only the first id matched will appear in the 'main app' screen.

  If Supercompany URL Scheme is installed in the project, apps that are found to be installed in the device, via their `AppUrlScheme`, are not matched.

  If there is no match, the 'main app' screen will be skipped and 'all apps' screen will appear instead.


## `CrossPromoInfo` fields
- `Id`: app identifier, will never be presented directly to users.
  + Used for matching main app ids.
  + Used in analytics events as the 'game' parameter.
  + Used by the texture caching system to format the cached file names.
- `Title`: app title, displayed in the 'all apps' screen.
- `IconUrl`: must be a valid URL pointing to a valid texture, or this app will not appear in the 'all apps' screen.
- `PortraitBannerUrl`: should be a valid URL pointing to a valid texture.
  This is used only on the 'main app' screen on portrait orientation.
  Its aspect should be 1:1.
- `LandscapeBannerUrl`: should be a valid URL pointing to a valid texture.
  This is used only on the 'main app' screen on landscape orientation.
  Its aspect should be 12:7.
- `AndroidPackage`: Android app package name.
  Used for opening Google Play Store intent in Android devices.
- `IosItunesId`: iOS iTunes app id.
  Used for opening the Apple App Store in iOS devices, as well as showing app promotion overlay on portrait orientation.
- `Url`: URL that will be opened when tapping the 'play now' button in the 'main app' screen or tapping any icon in 'all apps' screen.

  On Android this URL is not used.

  On iOS, if the URL is `null` but the iTunes id is not, an App Store URL will be used as fallback.
- `AppUrlScheme`: URL Scheme supported by the app.

  If Supercompany URL Scheme is installed in the project, This is used to verify if the app is installed in the device by querying if a URL with this scheme can be opened.



## Example remote config
```json
{
  "AppInfos": [
    {
      "Id": "app1",
      "Title": "My App number 1",
      "IconUrl": "https://domain.com/icons/app1",
      "PortraitBannerUrl": "https://domain.com/banner/app1.portrait.png",
      "LandscapeBannerUrl": "https://domain.com/banner/app1.landscape.png",
      "AndroidPackage": "com.domain.app1",
      "IosItunesId": "1234567890",
      "AppUrlScheme": "app1"
    },
    {
      "Id": "app2",
      "Title": "My App number 2",
      "IconUrl": "https://domain.com/icons/app2",
      "PortraitBannerUrl": "https://domain.com/banner/app2.portrait.png",
      "LandscapeBannerUrl": "https://domain.com/banner/app2.landscape.png",
      "AndroidPackage": "com.domain.app2",
      "IosItunesId": "1234567890",
      "AppUrlScheme": "app2"
    }
  ],
  "MainAppIds": ["app1", "app2"]
}
```