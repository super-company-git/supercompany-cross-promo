# Supercompany Cross Promo
Ready-made UI for promoting other apps inside Unity apps.

<img src="Extras~/main-screen-portrait.png" height="512" alt="Cross-Promo 'main app' screen"/>
<img src="Extras~/all-apps-portrait.png" height="512" alt="Cross-Promo 'all apps' screen"/>


## Dependencies
- [Supercompany Core](https://gitlab.com/super-company-git/supercompany-unity-core)
- (optional) [Supercompany Remote Config](https://gitlab.com/super-company-git/supercompany-remote-config): adds support for remote configuration for cross-promoted app information
- (optional) [Supercompany Analytics](https://gitlab.com/super-company-git/supercompany-analytics): automatic analytics event logging
- (optional) [Supercompany URL Scheme](https://gitlab.com/super-company-git/supercompany-url-scheme): support for checking if the cross-promoted apps are installed, so they can be ignored for main app promo


## Installing
Install this package via [Unity Package Manager](https://docs.unity3d.com/Manual/upm-ui-giturl.html) using the following URL:
```
https://gitlab.com/super-company-git/supercompany-cross-promo.git?path=/Packages/io.supercompany.cross-promo#1.0.5
```


## How to use
1. If the project uses Unity UI, add a [CrossPromoButtonUGUI prefab](Packages/io.supercompany.cross-promo/Prefabs/CrossPromoButtonUGUI.prefab) to your canvas.

   Otherwise, use the [CrossPromoButtonBase prefab](Packages/io.supercompany.cross-promo/Prefabs/CrossPromoButtonBase.prefab) instead and add graphics and interactivity to it manually.
   You will also need to setup the `UnityEvents` present for handling the setup events. Check out the [CrossPromoButtonUGUI prefab](Packages/io.supercompany.cross-promo/Prefabs/CrossPromoButtonUGUI.prefab) for a complete example.

   The [CrossPromoController](Packages/io.supercompany.cross-promo/Runtime/CrossPromoController.cs) component is the only object you need to use Cross-Promo functionality.
2. Configure the "Cross Promo Config" field of the `CrossPromoController` component.
   This can be done either in the Inspector, or via the `CrossPromoConfig` remote config if Supercompany Remote Config is installed in the project.

   For more information on the supported fields, check out the [CrossPromoConfig format](Extras~/CrossPromoConfig-format.md) documentation.
3. (optional) Mark "Setup On Start" so that the `CrossPromoController` makes its setup automatically on the object's `Start` message.
   The setup consists in waiting for the remote config to be initialized, if Supercompany Remote Config is installed in the project, and pre-downloading the textures that will be displayed.

   If you want to start the setup process programatically, call one of the `SetupAsync` method overloads.
4. (optional) Mark "Auto Open Once" if you want the Cross-Promo popup to open automatically once per installation.

   For greater control of when the popup opens automatically, call the `OpenPanel` or `OpenPanelCoroutine` methods manually.
   Use the `IsReadyToOpen` property or the "On Cross Promo Ready" event to make sure you only open the popup when it is ready to be shown.
   Use the `WasNeverOpened` property for tracking if the popup appeared once in the installation.
5. Enjoy 🍾


## Sample
Check out the [Sample Scene](Assets/Scenes/SampleScene.unity) for a sample implementation that uses Unity UI and remote config.
